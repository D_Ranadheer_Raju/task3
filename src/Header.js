import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
    <div  style={{backgroundColor: 'black'}}> 
        <Link to="/" >
            <h3>Register</h3>
        </Link>
        <div className="right menu">
            <Link to="/api/login" >
                <h3>Login</h3>
            </Link>
        </div>
    </div>
    );
};

export default Header;