import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import history from './history';
import * as jwt_decode from 'jwt-decode';

class UpdateProfile extends React.Component {
    state = {
        firstname: '',
        lastname: '',
        address: '',
        // email: '',
        token: '',
        decode: ''
    }
    decode = jwt_decode(localStorage.getItem('rana'));
    onFormSubmit = (event) => {
        event.preventDefault();
        axios.put('http://192.168.1.66:5000/api/profile', {
            // email: this.props.location.state.detail.email,
            token: localStorage.getItem('rana'),
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            address: this.state.address
        }).then((response) => {
            console.log(response)
            alert("You have successfully updated your details");
        }).catch(error => {
            console.log("Update Failure error:", error);
        })
        //this.setState({ email: this.props.location.state.detail.email });
    }
    logOut() {
        localStorage.removeItem('rana');
    }
    removeAccount() {
        axios.delete('http://192.168.1.66:5000/api/delete', {
            data: {
                token: localStorage.getItem('rana')
            }
        }).then((response) => {
            if (response.data.status) {
                localStorage.removeItem('rana');
                alert("You have successfully deleted  user");
                history.push('/');
            } else {
                alert("Please login to delete your account");
                history.push('/');
            }
        }).catch((error) => {
            console.log('Error: Failed request12312312312321 ', error)
        })
    }
    render() {
        return (
            <div>
                <div className="ui placeholder segment">
                    <Link to="/" onClick={this.logOut} className="btn btn-primary">Logout</Link>
                    <Link to="/" onClick={this.removeAccount} className="btn btn-danger">Delete</Link>
                    <h3>Welcome <b>{this.decode.data.email}</b></h3>
                    <form onSubmit={this.onFormSubmit} method="post">
                        <div className="form group">
                            <label>First Name:</label>
                            <input className="form-control" type="text"
                                value={this.state.firstname}
                                onChange={(event) => this.setState({ firstname: event.target.value })}
                                required />
                        </div>
                        <div className="form group">
                            <label>Last Name:</label>
                            <input className="form-control" type="text"
                                value={this.state.lastname}
                                onChange={(event) => this.setState({ lastname: event.target.value })}
                                required />
                        </div>
                        <div className="form-group">
                            <label>Address:</label>
                        </div>
                        <div className="ui left icon input">
                            <input className="form-control" type="text"
                                value={this.state.address}
                                onChange={(event) => this.setState({ address: event.target.value })} />
                            <i className="lock icon"></i>
                        </div>
                        <input className="btn btn-success" type="submit" value="Update" />
                    </form>
                </div>
            </div>
        )
    }
}
export default UpdateProfile;