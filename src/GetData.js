import React from 'react';
import axios from 'axios';
import UpdateProfile from './UpdateProfile';

class GetData extends React.Component {
    state = {
        email: '',
        password: ''
    }
    onFormSubmit = (event) => {
        event.preventDefault();
        axios.post('http://192.168.1.66:5000/api/login', {
            email: this.state.email,
            password: this.state.password
        }).then((response) => {
            console.log(response)
            if (response.data.isVerified) {

                localStorage.setItem('rana', response.data.token);
                alert("username = " + localStorage.getItem('rana'));
                this.props.history.push({
                    pathname: '/api/profile',
                    //search: `?query=${this.state.email}`,
                    state: { detail: response.data }
                })

            } else if (response.data.status) {
                alert("Please verify your email")

            } else {

                alert("Invalid credentials");
                this.setState({ email: '', password: '' });
            }
        }).catch((error) => {
            console.log("GetData Failure error:", error);
        })
    }
    render() {
        //       console.log("token found", localStorage.getItem('rana'), typeof localStorage.getItem('rana') )

        if (localStorage.getItem('rana') && localStorage.getItem('rana') !== "undefined") {
            //  console.log("token found", localStorage.getItem('rana') )
            //  console.log("hello")
            //    return (this.props.history.push({
            //         pathname: '/api/profile',
            //         //search: `?query=${this.state.email}`,
            //        // state: { detail: response.data }
            //     })
            return (
                <div>
                    <UpdateProfile />
                </div>
            );
        } else {
            // console.log("token not found")
            return (
                <div>
                    <div className="ui placeholder segment">
                        <h3>Login Form</h3>
                        <form onSubmit={this.onFormSubmit} method="post">
                            <div className="form group">
                                <label>Email:</label>
                                <input className="form-control" type="text"
                                    value={this.state.email}
                                    onChange={(event) => this.setState({ email: event.target.value })}
                                    required />
                            </div>
                            <div className="form-group">
                                <label>Password:</label>
                            </div>
                            <div className="ui left icon input">
                                <input className="form-control" type="password"
                                    value={this.state.password}
                                    onChange={(event) => this.setState({ password: event.target.value })}
                                    required />
                                <i className="lock icon"></i>
                            </div>
                            <input className="btn btn-success" type="submit" value="Login" />
                        </form>
                    </div>
                </div>
            )
        }
    }
}

export default GetData;