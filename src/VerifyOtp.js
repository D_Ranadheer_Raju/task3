import React from 'react';

class MobileOtp extends React.Component {
    state = {
        mobileotp: ''
    }
    onFormSubmit = (event) => {
        event.preventDefault();
       if(this.state.mobileotp === localStorage.getItem('otp')) {
           alert("You have successfully verified");
           localStorage.removeItem('otp');
           this.props.history.push('/api/login');
       } else {
           alert("Sorry, you entered wrong otp");
       }
    }
    render() {
        return (
            <div>
                    <div className="ui placeholder segment">
                        <h3>OTP Verification Form</h3>
                        <form onSubmit={this.onFormSubmit} method="post">
                            <div className="form group">
                                <label>Verify OTP:</label>
                                <input className="form-control" type="text"
                                    value={this.state.mobileotp}
                                    onChange={(event) => this.setState({ mobileotp: event.target.value })}
                                    placeholder="Please enter your 6 digit OTP number"
                                    required />
                            </div>
                            <input className="btn btn-success" type="submit" value="Verify" />
                        </form>
                    </div>
                </div>
        )
    }
}

export default MobileOtp;