import React from 'react';
import axios from 'axios'; 
//import history from './history';

class MobileOtp extends React.Component {
    state = {
        mobile: ''
    }
    onFormSubmit = (event) => {
        event.preventDefault();
        axios.post('http://192.168.1.66:5000/api/mobile', {
            mobile: '+91' + this.state.mobile
        }).then( (response) =>{
            console.log(response);
            localStorage.setItem('otp',response.data.otp);
            if(response.data.status) {

                alert("You have received otp");
               this.props.history.push('/api/mobileotp');
            } else {
                alert("Sorry, server problem");
            }
            //console.log(response);
        }).catch( (error) => {
            console.log("Mobile Error:", error);
        })
    }
    render() {
        return (
            <div>
                    <div className="ui placeholder segment">
                        <h3>Mobile Form</h3>
                        <form onSubmit={this.onFormSubmit} method="post">
                            <div className="form group">
                                <label>Mobile:</label>
                                <input className="form-control" type="text"
                                    value={this.state.mobile}
                                    onChange={(event) => this.setState({ mobile: event.target.value })}
                                    placeholder="Enter your 10 digit mobile number"
                                    required />
                            </div>
                            <input className="btn btn-success" type="submit" value="Submit" />
                        </form>
                    </div>
                </div>
        )
    }
}

export default MobileOtp;