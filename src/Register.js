import React from 'react';
import axios from 'axios';

class Register extends React.Component {
    state = {
        email: '',
        password: ''
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        axios.post('http://192.168.1.66:5000/api/register', {
            email: this.state.email,
            password: this.state.password,
            isVerified: false
        }).then((response) => {
            console.log(response.data.email)
            if (response && response.data && response.data.status) {
                alert(this.state.email + " have successfully registered");
                this.props.history.push('/api/login');
            } else {
                alert("This " + this.state.email + " is already registered");
                this.setState({ email: '', password: '' });
            }

        }).catch((error) => {
            console.log('Error: Failed request', error)
        })
    }

    render() {
        return (
            <div>
                <div className="ui placeholder segment">
                    <h3>Registration Form</h3>
                    <form onSubmit={this.onFormSubmit} method="post">
                        <div className="form group">
                            <label>Email:</label>
                            <input className="form-control" type="text"
                                value={this.state.email}
                                onChange={(event) => this.setState({ email: event.target.value })}
                                required />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div className="form-group">
                            <label>Password:</label>
                        </div>
                        <div className="ui left icon input">
                            <input className="form-control" type="password"
                                value={this.state.password}
                                onChange={(event) => this.setState({ password: event.target.value })}
                                required />
                            <i className="lock icon"></i>
                        </div>
                        <input className="btn btn-success" type="submit" value="Register" />
                    </form>
                </div>
            </div>
        );
    }
}

export default Register;