import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './Header';
import GetData from './GetData';
import Register from './Register';
import UpdateProfile from './UpdateProfile';
import MobileOtp from './MobileOtp';
import VerifyOtp from './VerifyOtp';

const App = () => {
    return (
        <div className="ui container"  style={{backgroundColor: 'wheat'}}>
            <Router >
                <div>
                    <Header />
                    <Route path="/" exact component={Register} />
                    <Route path="/api/login" exact component={GetData} />
                    <Route path="/api/profile" exact component={UpdateProfile} />
                    <Route path="/api/mobile" exact component={MobileOtp} />
                    <Route path="/api/mobileotp" exact component={VerifyOtp} />
                </div>
            </ Router>
        </div>
    );
};

export default App;
